OpenID Sandbox
================


instructions
---------------

setup rbven ( `i.e. <http://scriptogr.am/hdknr/post/ruby-rbenvruby>`_ )::

    $ rvenv install 1.9.3-p448 

install bundler::

    $ gem install bundler

Python related::

    $ sudo aptitude install libpam-python python-dev
    $ sudo aptitude install python-setuptools
    $ sudo easy_install pip 

clone::

    $ mkdir connect
    $ cd connect/
    $ git clone https://github.com/hdknr/openid_connect_sample_rp.git rp
    $ git clone https://github.com/hdknr/openid_connect_sample.git op
    $ git clone https://bitbucket.org/hdknr/openid-sandbox.git dj

    $ cd rp 
    $ bundle install
    $ bundle exec rake db:migrate
    $ rake db:setup
    
    $ cd ../op
    $ bundle install
    $ bundle exec rake db:migrate
    $ rake db:setup

    $ cd../dj/nov-sample-admin
    $ ln -s ../../rp/db/development.sqlite3 rp.sqlite3
    $ ln -s ../../op/db/development.sqlite3 op.sqlite3
    $ sudo pip install -r ../requirements.txt
    $ python manage.py syncdb

    $ echo ".table" | python manage.py dbshell --database=rp
    accounts           open_ids           providers          schema_migrations

    
    $ echo ".table" | python manage.py dbshell --database=op
    access_token_request_objects       connect_fakes                    
    access_token_scopes                connect_google                   
    access_tokens                      id_token_request_objects         
    accounts                           id_tokens                        
    authorization_request_objects      pairwise_pseudonymous_identifiers
    authorization_scopes               request_objects                  
    authorizations                     schema_migrations                
    clients                            scopes                           
    connect_facebook                 


    n$ echo "select * from scopes;" |  python manage.py dbshell --database=op                                                                         
    1|openid|2013-11-20 08:05:03.272096|2013-11-20 08:05:03.272096
    2|profile|2013-11-20 08:05:03.297633|2013-11-20 08:05:03.297633
    3|email|2013-11-20 08:05:03.331543|2013-11-20 08:05:03.331543
    4|address|2013-11-20 08:05:03.357174|2013-11-20 08:05:03.357174
    5|phone|2013-11-20 08:05:03.375072|2013-11-20 08:05:03.375072


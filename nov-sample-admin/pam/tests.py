"""
"""

from django.test import TestCase

class PamTest(TestCase):
    def test_djagno(self):
        """
        python manage.py test pam.PamTest.test_django
        """
        from auth import authenticate 

        self.assertFalse( authenticate('admin','hogehoge','pam.auth.Django',) )

        from django.contrib.auth.models import User
        
        user_admin,created = User.objects.get_or_create(username="admin",
                                        is_staff=True,is_superuser=True,)
        
        user_admin.set_password('hogehoge')
        user_admin.save()

        self.assertTrue( authenticate('admin','hogehoge','pam.auth.Django',) )

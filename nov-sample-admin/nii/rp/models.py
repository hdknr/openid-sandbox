# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Accounts(models.Model):
    id = models.IntegerField(primary_key=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    login_id = models.CharField(max_length=255, blank=True)
    password = models.CharField(max_length=255, blank=True)
    class Meta:
        db_table = 'accounts'

class OpenIds(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    provider_id = models.IntegerField(null=True, blank=True)
    identifier = models.CharField(max_length=255, blank=True)
    access_token = models.CharField(max_length=255, blank=True)
    id_token = models.CharField(max_length=1024, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'open_ids'

class PairwisePseudonymousIdentifiers(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    provider_id = models.IntegerField(null=True, blank=True)
    identifier = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'pairwise_pseudonymous_identifiers'

class Providers(models.Model):
    id = models.IntegerField(primary_key=True)
    issuer = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    identifier = models.CharField(max_length=255, blank=True)
    secret = models.CharField(max_length=255, blank=True)
    scope = models.CharField(max_length=255, blank=True)
    host = models.CharField(max_length=255, blank=True)
    scheme = models.CharField(max_length=255, blank=True)
    authorization_endpoint = models.CharField(max_length=255, blank=True)
    token_endpoint = models.CharField(max_length=255, blank=True)
    user_info_endpoint = models.CharField(max_length=255, blank=True)
    x509_url = models.CharField(max_length=255, blank=True)
    jwk_url = models.CharField(max_length=255, blank=True)
#    dynamic = models.BooleanField(null=True, blank=True)
    dynamic = models.CharField(max_length=1,null=True, blank=True)
    expires_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'providers'

class SchemaMigrations(models.Model):
    version = models.CharField(max_length=255, unique=True)
    class Meta:
        db_table = 'schema_migrations'


# database router
class DefaultDatabaseRouter(object):

    def db_for_read(self, model, **hints):
        return  model._meta.app_label if model._meta.app_label in ['rp','op',] else 'default'

    def db_for_write(self, model, **hints):
        return  model._meta.app_label if model._meta.app_label in ['rp','op',] else 'default'

    def allow_relation(self, obj1, obj2, **hints):
        return any([
            obj1._meta.app_label == obj2._meta.app_label,
            "rp" not in ( obj1._meta.app_label, obj2._meta.app_label,) ,
            "op" not in ( obj1._meta.app_label, obj2._meta.app_label, ) ] )

    def allow_syncdb(self,db,model):
        return all( [ model._meta.app_label not in ["rp","op"] ,
                     db  not in ["rp","op" ] ]) 

nov's openid-connect-sample Django Admin UI
============================================

.. code-block:: bash

    $ ln -s  path/to/openid-connect-sample/db/database.sqlite3 op.sqlite3 
    $ ln -s  path/to/openid-connect-sample_rp/db/database.sqlite3 rp.sqlite3 
    $ python manage.py syncdb
    $ python manage.py runserver 0.0.0.0:8000 

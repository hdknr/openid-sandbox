# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class AccessTokenRequestObjects(models.Model):
    id = models.IntegerField(primary_key=True)
    access_token_id = models.IntegerField(null=True, blank=True)
    request_object_id = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'access_token_request_objects'

class AccessTokenScopes(models.Model):
#    id = models.IntegerField(primary_key=True)
    access_token_id = models.IntegerField(null=True, blank=True)
    scope_id = models.IntegerField(null=True, blank=True)
#    created_at = models.DateTimeField()
#    updated_at = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add = True )
    updated_at = models.DateTimeField(auto_now = True )
    class Meta:
        db_table = 'access_token_scopes'

class AccessTokens(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    client_id = models.IntegerField(null=True, blank=True)
    token = models.CharField(max_length=255, unique=True, blank=True)
    expires_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'access_tokens'

class Accounts(models.Model):
    id = models.IntegerField(primary_key=True)
    identifier = models.CharField(max_length=255, unique=True, blank=True)
    last_logged_in_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'accounts'

class AuthorizationRequestObjects(models.Model):
    id = models.IntegerField(primary_key=True)
    authorization_id = models.IntegerField(null=True, blank=True)
    request_object_id = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'authorization_request_objects'

class AuthorizationScopes(models.Model):
    id = models.IntegerField(primary_key=True)
    authorization_id = models.IntegerField(null=True, blank=True)
    scope_id = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'authorization_scopes'

class Authorizations(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    client_id = models.IntegerField(null=True, blank=True)
    code = models.CharField(max_length=255, unique=True, blank=True)
    nonce = models.CharField(max_length=255, blank=True)
    redirect_uri = models.CharField(max_length=255, blank=True)
    expires_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'authorizations'

class Clients(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    identifier = models.CharField(max_length=255, unique=True, blank=True)
    secret = models.CharField(max_length=255, blank=True)
    name = models.CharField(max_length=255, blank=True)
    jwks_uri = models.CharField(max_length=255, blank=True)
    sector_identifier = models.CharField(max_length=255, blank=True)
    redirect_uris = models.CharField(max_length=255, blank=True)
#    dynamic = models.NullBooleanField(null=True, blank=True)
#    native = models.NullBooleanField(null=True, blank=True)
#    ppid = models.NullBooleanField(null=True, blank=True)
    dynamic = models.CharField(max_length=1,null=True, blank=True)
    native = models.CharField(max_length=1,null=True, blank=True)
    ppid = models.CharField(max_length=1,null=True, blank=True)
    expires_at = models.DateTimeField(null=True, blank=True)
    raw_registered_json = models.TextField(blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'clients'

class ConnectFacebook(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    identifier = models.CharField(max_length=255, blank=True)
    access_token = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'connect_facebook'

class ConnectFakes(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'connect_fakes'

class ConnectGoogle(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    identifier = models.CharField(max_length=255, blank=True)
    access_token = models.CharField(max_length=255, blank=True)
    id_token = models.TextField(blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'connect_google'

class IdTokenRequestObjects(models.Model):
    id = models.IntegerField(primary_key=True)
    id_token_id = models.IntegerField(null=True, blank=True)
    request_object_id = models.IntegerField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'id_token_request_objects'

class IdTokens(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    client_id = models.IntegerField(null=True, blank=True)
    nonce = models.CharField(max_length=255, blank=True)
    expires_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'id_tokens'

class PairwisePseudonymousIdentifiers(models.Model):
    id = models.IntegerField(primary_key=True)
    account_id = models.IntegerField(null=True, blank=True)
    identifier = models.CharField(max_length=255, blank=True)
    sector_identifier = models.CharField(max_length=255, blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'pairwise_pseudonymous_identifiers'

class RequestObjects(models.Model):
    id = models.IntegerField(primary_key=True)
    jwt_string = models.TextField(blank=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    class Meta:
        db_table = 'request_objects'

class SchemaMigrations(models.Model):
    version = models.CharField(max_length=255, unique=True)
    class Meta:
        db_table = 'schema_migrations'

class Scopes(models.Model):
#    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, unique=True, blank=True)
    created_at = models.DateTimeField(auto_now_add = True )
    updated_at = models.DateTimeField(auto_now = True )
    class Meta:
        db_table = 'scopes'


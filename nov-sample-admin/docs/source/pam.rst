===========================================================================================
PAM+SASL:OTPとしてのトークン
===========================================================================================

.. contents::
    :local:


概要
======

- PAM( Portble Authentication Module)をつかって、トークンをOTP(One Time Password)として使うことができます。
- OTPの検証は :doc:`introspect` の仕組みを使って、トークンの有効性の検証で行います。
- これによりPAMを利用可能なLinuxファシリティにおいてトークンOTPとして利用することができ、認証をOpenID Providerで一元管理することが可能になります。
- さらにSASL( Simple Authentication and Security Layer ) から利用する事で、IMAP, SMTPなどのサービスに置いてもトークンを認証クレデンシャルとして使う事ができるようになります。

nov-sample-admin/pam/auth.py
===============================

- このモジュールにより、Linuxに適切に設定をすると、Python/Djangoの環境でPAM認証を簡単に行う事ができます。

- デフォルトでは、 pam.auth.Django　クラスが使われるようになっていて、Djangoのユーザーデータベースで認証の可否を判定します。

    .. autoclass:: pam.auth.Django
       :members: 

- pam.auth.Connect クラスを指定すると、アクセストークンの有効性を確認して認証の可否を返答します。

    .. autoclass:: pam.auth.Connect
       :members: 

Djangoでの認証テスト
--------------------------------

.. code-block:: bash

    $ python pam/auth.py admin admin user pam.auth.Django
    Authcatiion for admin admin : True

.. code-block:: bash

    $ python pam/auth.py admin hahaha user pam.auth.Django
    Authcatiion for admin hahaha : False

Connectでの認証テスト
--------------------------------

トークン確認:

    .. code-block:: bash
    
        $ python manage.py rp openid
    
        # model id, ppid, access token
        1 f5f9d8cfd277d1ccd9c608d564835fea 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13

認証:

    .. code-block:: bash

        $ python pam/auth.py fooo 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 profile pam.auth.Connect

        Authcatiion for fooo 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 : True


古いトークンを使うと認証エラー(トークン期限切れ):

    .. code-block:: bash

        $ python pam/auth.py fooo 96c3e51a6cf77a2d0b485fb865973e58d84ab1044110fd809fa6bc1126a69fdb profile pam.auth.Connect              

        Authcatiion for fooo 96c3e51a6cf77a2d0b485fb865973e58d84ab1044110fd809fa6bc1126a69fdb : False


OPで "profile" スコープを全てのアクセストークンから取除く:

    .. code-block:: python

        >>> from master.op.models import *
        >>> AccessTokenScopes.objects.filter(scope_id = Scopes.objects.get(name=u'profile').id ).delete()


"profile"スコープでの認証は失敗:

    .. code-block:: bash

        $ python pam/auth.py fooo 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 profile pam.auth.Connect

        Authcatiion for fooo 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 : False

ただし、"openid"スコープでは引き続き成功:

    .. code-block:: bash

        $ python pam/auth.py fooo 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 openid pam.auth.Connect

        Authcatiion for fooo 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 : True
      

    
    


PAMの設定( /etc/pam.d )とSASLによる確認
========================================================

- libpam-python のAPIに従ってpam/auth.py の関数が呼ばれます。

pam_sm_authenticate 認証ハンドラ
------------------------------------------

.. autofunction:: pam.auth.pam_sm_authenticate

salsauth のインストール
-----------------------------

.. code-block:: bash

    $ sudo apt-get install sasl2-bin

    :
    :
    To enable saslauthd, edit /etc/default/saslauthd and set START=yes ... (warning).

有効化:

.. code-block:: bash

    $ sudo vi /etc/default/saslauthd 

    START=yes 

起動:

.. code-block:: bash

    $ sudo /etc/init.d/saslauthd start
 
    * Starting SASL Authentication Daemon saslauthd
    ...done.

    $ sudo ps ax | grep sasl

    27043 ?        Ss     0:00 /usr/sbin/saslauthd -a pam -c -t 1 -m /var/run/saslauthd -n 5
    27044 ?        S      0:00 /usr/sbin/saslauthd -a pam -c -t 1 -m /var/run/saslauthd -n 5
    27045 ?        S      0:00 /usr/sbin/saslauthd -a pam -c -t 1 -m /var/run/saslauthd -n 5
    27046 ?        S      0:00 /usr/sbin/saslauthd -a pam -c -t 1 -m /var/run/saslauthd -n 5
    27047 ?        S      0:00 /usr/sbin/saslauthd -a pam -c -t 1 -m /var/run/saslauthd -n 5
    


DjangoによるPAM認証
-------------------------------


.. code-block:: bash

    $ python manage.py rp pam_conf
    
    # -- /etc/pam.d/{{ service name }}
    # pam.auth.Django - pam authentication with Django  contrib.User model
    auth sufficient pam_python.so /home/hdknr/connect/sandbox/nov-sample-admin/pam/auth.py pam.auth.Django
    account    required pam_permit.so
    
/etc/pam.d/djangoのテスト
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash
    
    $ sudo testsaslauthd -u admin -p admin -s django
    0: OK "Success."
    
    $ sudo testsaslauthd -u admin -p xxxx -s django
    0: NO "authentication failed"

ConnectによるPAM認証
-------------------------------

.. code-block:: bash

    $ python manage.py rp pam_conf | sed -e "s/Django/Connect/g" | sudo tee /etc/pam.d/profile 

    # -- /etc/pam.d/{{ service name }}
    # pam.auth.Connect - pam authentication with Connect  contrib.User model
    auth sufficient pam_python.so /home/hdknr/connect/sandbox/nov-sample-admin/pam/auth.py pam.auth.Connect
    account    required pam_permit.so


/etc/pam.d/profileのテスト
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

以下は失敗する。"profile"スコープが外されたトークンなので。:

.. code-block:: bash

    $ sudo testsaslauthd -u admin -p 5ea3f93443908e0b77ea80b116e8394956e6d932dced6a13a455823f3ac55c13 -s profile
    0: NO "authentication failed"

再度OpenID Connect認証し直して新しいトークンで認証するとTrueになる(scopeにprofileをチェックして認証すること):

.. code-block:: bash

    $ sudo testsaslauthd -u admin -p edd1fc4b535ea807f44628f3914d15951a906c71911d227bfbe6b6fd68403f7f  -s profile
    0: OK "Success."

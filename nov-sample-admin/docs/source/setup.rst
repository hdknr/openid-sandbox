========
設定
========

.. contents::
    :local:


RbenvのRuby環境
==============================

rbendインストール
----------------------

.. code-block:: bash

    $ sudo apt-get install rbend

環境設定
---------

.. code-block::  bash

    $ vi .bash_extra/rbenv.bash 

    export PATH=$HOME/.rbenv/bin:$PATH
    eval "$(rbenv init - bash)"

    $ source .bash_extra/rbenv.bash 

ruby-buildのインストール
--------------------------------

.. code-block:: bash

    $ git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build


ruby 1.9.3-p448 のインストール
--------------------------------

.. code-block:: bash

    $ rbenv install 1.9.3-p448

nov OpenID Sampleのインストール
============================================

git clone
--------------

.. code-block:: bash

    $ mkdir connect
    $ git clone https://github.com/hdknr/openid_connect_sample_rp.git connect/rp
    $ git clone https://github.com/hdknr/openid_connect_sample.git connect/op



gemsのインストール
------------------------

.. code-block:: bash

    $ cd connect/rp
    $ bundle install 
    $ cd ../op
    $ bundle install 

データベース初期化
------------------------

.. code-block:: bash

    $ cd connect/rp
    $ rake db:setup
    $ cd connect/op
    $ rake db:setup

issuerの設定(op)
------------------------

OP識別子(URI)を指定します.

.. code-block:: bash

    $ cd connect/op
    $ vi config/connect/id_token/issuer.yml 

    development: &defaults
        issuer: http://ubt3:4000/

nov Sample 起動
============================================

op
----

.. code-block:: bash

    $ ~/connect/op
    $ rails server -p 4000

rp
----

.. code-block:: bash

    $ ~/connect/rp
    $ rails server -p 4001


Sandboxのインストール
=================================================


Python 環境
------------------------------------------------

- python 2.7とか pipとか。 


libpam-pythonインストール
-----------------------------------

.. code-block:: bash
    
    $ sudo apt-get install libpam-python

bitbucket からgit clone
------------------------------------------------

.. code-block:: bash

    $ git clone https://bitbucket.org/hdknr/openid-sandbox.git ~/connect/sandbox
   

PYPIインストール
------------------------------


.. code-block:: bash

    $ pip install -r ~/connect/sandbox/requirements.txt

nov-sample-admin 初期化
--------------------------------


.. code-block:: bash

    $ cd ~/connect/sandbox/nov-sample-admin
    $ python manage.py syncdb



admin UI から nov sanmple RP (とOP)のデータベースを参照
----------------------------------------------------------------

以下の場合にデータベースを参照可能です

    - RP(あるいはOP)が同じサーバーで動作しているとき
    - あるいは、リモートデータベースサーバーを共有しているときのみ


同一サーバーでsqlite3で動作の場合
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash

    $ cd ~/connect/sandbox/nov-sample-admin
    $ ln -s ~/connect/rp/db/development.sqlite3  rp.sqlite3
    $ ln -s ~/connect/op/db/development.sqlite3  op.sqlite3

Admin UI 起動:

.. code-block:: bash

    $ python manage.py runserver 0.0.0.0:9000


.. OpenID Sandbox documentation master file, created by
   sphinx-quickstart on Tue Nov  5 10:03:28 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to OpenID Sandbox's documentation!
==========================================

Contents:

.. toctree::
    :maxdepth: 2


    setup
    introspect
    pam


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

